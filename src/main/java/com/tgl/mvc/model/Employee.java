package com.tgl.mvc.model;

import javax.validation.constraints.Email;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel("員工基本資料")
public class Employee {

	@ApiModelProperty(value = "員工编號", required = true)
	private long id;

	@ApiModelProperty(value = "身高", required = true)
	private float high;

	@ApiModelProperty(value = "體重", required = true)
	private float weight;

	@ApiModelProperty(value = "英文姓名", required = true)
	private String ename;

	@ApiModelProperty(value = "中文姓名", required = true)
	private String cname;

	@ApiModelProperty(value = "分機", required = false)
	private String ext;

	@ApiModelProperty(value = "e-mail", required = false)
	@Email(message = "e-mail 有誤")
	private String email;

	@ApiModelProperty(value = "BMI", required = false)
	private float bmi;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public float getHigh() {
		return high;
	}

	public void setHigh(float high) {
		this.high = high;
	}

	public float getWeight() {
		return weight;
	}

	public void setWeight(float weight) {
		this.weight = weight;
	}

	public String getEname() {
		return ename;
	}

	public void setEname(String ename) {
		this.ename = ename;
	}

	public String getCname() {
		return cname;
	}

	public void setCname(String cname) {
		this.cname = cname;
	}

	public String getExt() {
		return ext;
	}

	public void setExt(String ext) {
		this.ext = ext;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public float getBmi() {
		return bmi;
	}

	public void setBmi(float bmi) {
		this.bmi = bmi;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", high=" + high + ", weight=" + weight + ", ename=" + ename + ", cname=" + cname
				+ ", ext=" + ext + ", email=" + email + ", bmi=" + bmi + "]";
	}

}
