package com.tgl.mvc.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tgl.mvc.dao.EmployeeDao;
import com.tgl.mvc.model.Employee;
import com.tgl.mvc.util.Util;

@Service
public class EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;

	public Employee inquiryId(long employeeId) {
		Employee rs = employeeDao.inquiryId(employeeId);
		if (rs == null) {
			return null;
		}
		Util util = new Util();
		rs.setCname(util.maskutil(rs.getCname()));
		return rs;
	} // * inquiryId

	public long insert(Employee employee) {
		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		return employeeDao.insert(employee);
	} // end insert

	public Employee update(Employee employee) {
		Util util = new Util();
		float vBmi = util.calcBmi(employee.getHigh(), employee.getWeight());
		employee.setBmi(vBmi);
		return employeeDao.update(employee);
	}

	public boolean delete(long employeeId) {
		return employeeDao.delete(employeeId);
	}

}
