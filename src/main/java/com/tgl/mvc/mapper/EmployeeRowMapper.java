package com.tgl.mvc.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.tgl.mvc.model.Employee;

public class EmployeeRowMapper implements RowMapper<Employee> {
	public Employee mapRow(ResultSet rs, int recNum) throws SQLException {
		Employee emp = new Employee();
		emp.setId(rs.getLong("id"));
		emp.setCname(rs.getString("cname"));
		emp.setEname(rs.getString("ename"));
		emp.setEmail(rs.getString("email"));
		emp.setExt(rs.getString("ext"));
		emp.setHigh(rs.getFloat("high"));
		emp.setWeight(rs.getFloat("weight"));
		emp.setBmi(rs.getFloat("bmi"));

		return emp;
	}
}
