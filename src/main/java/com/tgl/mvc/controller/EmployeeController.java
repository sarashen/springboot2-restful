package com.tgl.mvc.controller;

import javax.validation.Valid;
import javax.validation.constraints.Min;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.tgl.mvc.model.Employee;
import com.tgl.mvc.service.EmployeeService;

import io.swagger.annotations.ApiOperation;

@RestController // RESTful API
@ResponseBody
@RequestMapping(value = "/sara/employee")
public class EmployeeController {

	@Autowired
	private EmployeeService employeeService;
	Logger logger = Logger.getLogger(EmployeeController.class);

	@ApiOperation("查詢員工資料接口")
	@GetMapping(value = "/{id}") // Inquiry
	public ResponseEntity<Employee> inquiryId(@PathVariable("id") @Min(1) long employeeId) {
		Employee rs = employeeService.inquiryId(employeeId);

		if (rs == null) {
			logger.debug("Data not found , id->:" + employeeId);
		}
		return new ResponseEntity<>(rs, HttpStatus.OK);

	}

	@ApiOperation("新增員工接口")
	@PostMapping // Insert data
	public ResponseEntity<Long> insert(@RequestBody @Valid Employee employee) {

		logger.debug("employee:" + employee);

		long insertId = employeeService.insert(employee);
		return new ResponseEntity<>(insertId, HttpStatus.CREATED);
	}

	@ApiOperation("維護員工資料接口")
	@PutMapping
	public ResponseEntity<Employee> update(@RequestBody @Valid Employee employee) {

		Employee rs = employeeService.inquiryId(employee.getId());

		if (rs == null) {
			logger.info("Record not found, id:" + employee.getId());
		}
		employeeService.update(employee);
		return new ResponseEntity<>(employee, HttpStatus.OK);
	}

	@ApiOperation("刪除員工資料接口")
	@DeleteMapping(value = "/{id}") // Delete Data
	public ResponseEntity<Long> delete(@PathVariable("id") @Min(1) long employeeId) {

		Employee rs = employeeService.inquiryId(employeeId);
		if (rs == null) {
			logger.info("Record not found, id:" + employeeId);
		}
		employeeService.delete(employeeId);
		return new ResponseEntity<>(employeeId, HttpStatus.OK);
	}

} // end class
